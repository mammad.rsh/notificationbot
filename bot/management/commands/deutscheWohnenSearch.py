from django.core.management.base import BaseCommand, CommandError
import requests
import telegram
import time
import redis
from datetime import datetime

bot = telegram.Bot("1958727993:AAGzGXBxDjnd2lqSbMtnrGjbT-PTO_XaJa8")
r = redis.Redis()

class Command(BaseCommand):
    help = 'Searching on Deutsche Wohnen Apartments!'

    def handle(self, *args, **options):
        while(True):
            search()
            time.sleep(30)

        self.stdout.write(self.style.SUCCESS('Finished!'))

def search():
    try:
        print("New search... " + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
        data = {"infrastructure":{},"flatTypes":{},"other":{},"page":"1","locale":"de","commercializationType":"rent","utilizationType":"flat,retirement","city":"Berlin","location":"Berlin"}
        result = requests.post('https://immo-api.deutsche-wohnen.com/estate/findByFilter', json=data ,timeout=5)
        for w in result.json():
            if r.hget("ids", w["id"]) == None:
                sendApartmentDetails(w)
                r.hset("ids", w["id"], "")
                
    except e:
        print (e.getMessage())

def sendApartmentDetails(w):
    telegramId = 123858837
    wLink = "https://www.deutsche-wohnen.com/expose/object/" + w["id"]
    wId = w["id"]
    wPrice = ''
    wDistrict = ''
    wZip = ''
    wArea = ''
    try:
        wPrice = w["price"]
        wDistrict = w["address"]["district"]
        wZip = w["address"]["zip"]
        wArea = w["area"]
    except:
        print ("error")
    text = 'New Apartment found id : {0}, price: <b>{1}</b>, address: {2}, zip: {3} area: {4}, <a href="{5}">Link</a>'.format(
        wId, 
        wPrice,
        wDistrict,
        wZip, 
        wArea, 
        wLink)
    print(text)
    parse_mode = "HTML"
    bot.sendMessage(telegramId, text, parse_mode)